﻿using System.ComponentModel.DataAnnotations;

namespace Avaukro.Data;

public class Category
{
	[Key]
	public required string Name { get; set; }
}
