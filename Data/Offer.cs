﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Avaukro.Data;

#pragma warning disable
public class Offer
{
	public Guid Id { get; set; }

	[Required]
	public required string Title { get; set; }

	[Required]
	public required string Description { get; set; }

	[Required]
	public required User Seller { get; set; }

	[Range(0, int.MaxValue)]
	public required int Price { get; set; }

	[Required]
	public required Category Category;

	public Bid HighestBid { get; set; }

	public required ICollection<Bid> Bids = new List<Bid>();

	[Required]
	public required DateTime DateListed { get; set; }

	public DateTime? DateSold { get; set; }

}
#pragma warning enable
