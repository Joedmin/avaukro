﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Avaukro.Data;

public class Bid
{
	[Key]
	public required Guid OfferId { get; set; }

	public required User User { get; set; }

	[Required]
	public required int Amount { get; set; }

	public override string ToString()
	{
		return $"{Amount} by {User}.";
	}
}
