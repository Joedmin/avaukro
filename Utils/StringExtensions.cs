﻿using System.Security.Cryptography;
using System.Text;

namespace Avaukro.Utils;

public static class StringExtensions
{
	public static string GetSHA256Hash(this string rawData)
	{
		byte[] bytes = SHA256.HashData(Encoding.UTF8.GetBytes(rawData));
		return Encoding.UTF8.GetString(bytes);
	}
}
