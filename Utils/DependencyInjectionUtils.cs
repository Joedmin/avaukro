﻿using System.Reflection;
using Avaukro.Data;
using Avaukro.Services;
using ReactiveUI;
using Splat;

namespace Avaukro.Utils;

internal static class DependencyInjectionUtils
{
	public static void Register(IMutableDependencyResolver services, IReadonlyDependencyResolver resolver)
	{
		services.RegisterLazySingleton(() => new ApplicationDbContext());
		services.RegisterLazySingleton(() =>
			new UserManager(resolver.GetRequiredService<ApplicationDbContext>()));

		services.RegisterViewsForViewModels(Assembly.GetCallingAssembly());
	}

	public static T GetRequiredService<T>()
		where T : class
		=> Locator.Current.GetRequiredService<T>();
}
