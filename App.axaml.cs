using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avaukro.Data;
using Avaukro.Utils;
using Avaukro.ViewModels;
using Avaukro.Views;
using Splat;

namespace Avaukro;

public class App : Application
{
	public override void Initialize()
	{
		AvaloniaXamlLoader.Load(this);
	}

	public override void OnFrameworkInitializationCompleted()
	{
		DependencyInjectionUtils.Register(Locator.CurrentMutable, Locator.Current);
		ApplicationDbContext.Migrate();

		if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
		{
			desktop.MainWindow = new MainWindow
			{
				DataContext = new MainWindowViewModel()
			};
		}

		base.OnFrameworkInitializationCompleted();
	}
}
