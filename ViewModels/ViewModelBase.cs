﻿using System;
using Avaukro.Services;
using Avaukro.Utils;
using ReactiveUI;
using Splat;

namespace Avaukro.ViewModels;

public class ViewModelBase : ReactiveObject
{
	protected readonly Action<ViewModelBase> navigateToAction;
	protected readonly UserManager userManager;

	public ViewModelBase(Action<ViewModelBase> navigateToAction)
	{
		this.navigateToAction = navigateToAction;
		userManager = Locator.Current.GetRequiredService<UserManager>();
	}
}
