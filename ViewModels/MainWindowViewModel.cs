﻿using System;
using System.Diagnostics;
using ReactiveUI;

namespace Avaukro.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
	private ViewModelBase content;
	public ViewModelBase Content
	{
		get => content;
		private set => this.RaiseAndSetIfChanged(ref content, value);
	}

	public MainWindowViewModel()
		: base(DoNothing)
	{
		Content = new HomeViewModel(ChangeView);
	}

	private static void DoNothing(ViewModelBase viewModel)
	{
		// Just so I can pass something to the base
	}

	public void ChangeView(ViewModelBase viewModel)
		=> Content = viewModel;

	public void ShowItem(object parameter)
	{
		Debug.WriteLine("Navigating to offer details");
		Content = new ItemDetailsViewModel(ChangeView, (Guid)parameter);
	}
}
