using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using Avaukro.Data;
using Avaukro.Utils;
using Microsoft.EntityFrameworkCore;
using Splat;

namespace Avaukro.ViewModels;

public class ItemDetailsViewModel : ViewModelBase, INotifyPropertyChanged
{
	private readonly ApplicationDbContext context;

	private Offer offer;
	public Offer Offer
	{
		get => offer;
		set
		{
			if (offer != value)
			{
				offer = value;
				OnPropertyChanged(nameof(offer));
			}
		}
	}

	public readonly bool IsSeller;
	public bool CanBet => !IsSeller && userManager.UserLoggedIn;

	public User Seller => Offer.Seller;
	public string Title => Offer.Title;
	public DateTime DateListed => Offer.DateListed;
	public Bid HighestBid => Offer.HighestBid;

	[Required]
	[Range(0, int.MaxValue)]
	public int BidValue { get; set; } = 0;

	private string errorMessage = "";
	public string ErrorMessage
	{
		get => errorMessage;
		set
		{
			if (errorMessage != value)
			{
				errorMessage = value;
				OnPropertyChanged(nameof(ErrorMessage));
			}
		}
	}

	public new event PropertyChangedEventHandler? PropertyChanged;

	public ItemDetailsViewModel(Action<ViewModelBase> navigateToAction, Guid id)
		: base(navigateToAction)
	{
		context = Locator.Current.GetRequiredService<ApplicationDbContext>();
		Offer = context.Offers.Include(x => x.Seller).Include(x => x.HighestBid).ThenInclude(x => x.User).FirstOrDefault(x => x.Id == id)
			?? throw new ArgumentException($"Offer with id '{id}' not found.");
		IsSeller = userManager.CurrentUser == Offer.Seller;
		BidValue = Offer.Price + 1;
	}

	protected virtual void OnPropertyChanged(string propertyName)
	{
		PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	}

	public void GoHome()
	{
		Debug.WriteLine("Navigating to home");
		navigateToAction(new HomeViewModel(navigateToAction));
	}

	public void Bid()
	{
		ErrorMessage = "";
		if (BidValue <= Offer.HighestBid.Amount)
		{
			ErrorMessage = "Bid cannot be smaller than the highest one!";
			return;
		}

		context.Update(Offer).State = EntityState.Modified;

		var bid = new Bid
		{
			Amount = BidValue,
			OfferId = Offer.Id,
			User = userManager.CurrentUser!,
		};

		Offer.Bids.Add(bid);
		Offer.HighestBid = bid;

		context.SaveChanges();
		GoHome();
	}
}
