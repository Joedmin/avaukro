using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Avaukro.Data;
using Avaukro.Utils;
using ReactiveUI;
using Splat;

namespace Avaukro.ViewModels;

public class CreateOfferViewModel : ViewModelBase, INotifyPropertyChanged
{
	private readonly ApplicationDbContext context;

	public Offer Model { get; set; } = Activator.CreateInstance<Offer>()!; // I don't want to set the required members over here

	public List<Category> AvailableCategories { get; set; }

	private Category selectedCategory;
	public Category SelectedCategory
	{
		get => selectedCategory;
		set => this.RaiseAndSetIfChanged(ref selectedCategory, value);
	}

	private string errorMessage = "";
	public string ErrorMessage
	{
		get => errorMessage;
		set
		{
			if (errorMessage != value)
			{
				errorMessage = value;
				OnPropertyChanged(nameof(ErrorMessage));
			}
		}
	}

	public new event PropertyChangedEventHandler? PropertyChanged;

	public CreateOfferViewModel(Action<ViewModelBase> navigateToAction)
		: base(navigateToAction)
	{
		context = Locator.Current.GetRequiredService<ApplicationDbContext>();
		AvailableCategories = context.Categories.ToList();
	}

	protected virtual void OnPropertyChanged(string propertyName)
	{
		PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	}

	public void Create()
	{
		if (userManager.CurrentUser is null)
		{
			throw new InvalidOperationException("User not logged in. Cannot create offer.");
		}

		Model.DateListed = DateTime.Now;
		Model.Seller = userManager.CurrentUser!;
		context.Offers.Add(Model);
		context.SaveChanges();
		GoHome();
	}

	public void GoHome()
	{
		Debug.WriteLine("Navigating to home");
		navigateToAction(new HomeViewModel(navigateToAction));
	}
}
