using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using Avaukro.Services;
using GalaSoft.MvvmLight.Command;

namespace Avaukro.ViewModels;

public class LoginViewModel : ViewModelBase, INotifyPropertyChanged
{
	private ICommand loginCommand;
	public ICommand LoginCommand
		=> loginCommand ??= new RelayCommand(Login);

	private ICommand registerCommand;
	public ICommand RegisterCommand
		=> registerCommand ??= new RelayCommand(Register);

	private string errorMessage = "";
	public string ErrorMessage
	{
		get => errorMessage;
		set
		{
			if (errorMessage != value)
			{
				errorMessage = value;
				OnPropertyChanged(nameof(ErrorMessage));
			}
		}
	}

	private bool isRegistration;
	public bool IsRegistration
	{
		get => isRegistration;
		set
		{
			if (isRegistration != value)
			{
				isRegistration = value;
				OnPropertyChanged(nameof(IsRegistration));
			}
		}
	}

	public string FirstName { get; set; }
	public string LastName { get; set; }
	public string UserName { get; set; }
	public string Password { get; set; }

	public new event PropertyChangedEventHandler? PropertyChanged;

	public LoginViewModel(Action<ViewModelBase> navigateToAction)
		: base(navigateToAction)
	{
	}

	protected virtual void OnPropertyChanged(string propertyName)
	{
		PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	}

	public async void Login()
	{
		if (UserName is null || Password is null)
		{
			IsRegistration = false;
			ErrorMessage = "Both username and password are required!";
			return;
		}

		try
		{
			await userManager.LoginAsync(UserName, Password).ConfigureAwait(true);
		}
		catch (InvalidLoginException ex)
		{
			ErrorMessage = ex.Message;
			return;
		}

		Debug.WriteLine("Navigating to home");
		navigateToAction(new HomeViewModel(navigateToAction));
	}

	public async void Register()
	{
		if (FirstName is null || LastName is null)
		{
			IsRegistration = true;
			ErrorMessage = "Full name is required!";
			return;
		}

		if (UserName is null || Password is null)
		{
			ErrorMessage = "Both username and password are required!";
			return;
		}

		try
		{
			await userManager.RegisterAsync(FirstName, LastName, UserName, Password).ConfigureAwait(true);
		}
		catch (InvalidLoginException ex)
		{
			ErrorMessage = ex.Message;
			return;
		}

		Debug.WriteLine("Navigating to home");
		navigateToAction(new HomeViewModel(navigateToAction));
	}

	public void GoHome()
	{
		Debug.WriteLine("Navigating to home");
		navigateToAction(new HomeViewModel(navigateToAction));
	}
}
