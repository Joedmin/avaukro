﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using Avaukro.Data;
using Avaukro.Utils;
using Microsoft.EntityFrameworkCore;
using ReactiveUI;
using Splat;

namespace Avaukro.ViewModels;

public class HomeViewModel : ViewModelBase
{
	private readonly ApplicationDbContext context;

	private ObservableCollection<Offer> offers;
	public ObservableCollection<Offer> Offers
	{
		get => offers;
		set => this.RaiseAndSetIfChanged(ref offers, value);
	}

	public bool IsLoggerIn => userManager.UserLoggedIn;

	public HomeViewModel(Action<ViewModelBase> navigation)
		: base(navigation)
	{
		context = Locator.Current.GetRequiredService<ApplicationDbContext>();
		Offers = new(context.Offers.Include(x => x.Seller).Include(x => x.HighestBid).ThenInclude(x => x.User).ToList());
	}

	public void GoToLogin()
	{
		Debug.WriteLine("Navigating to login");
		navigateToAction(new LoginViewModel(navigateToAction));
	}

	public void Logout()
	{
		userManager.LogOut();
		this.RaisePropertyChanged(nameof(IsLoggerIn));
	}

	public void CreateOffer()
	{
		Debug.WriteLine("Navigating to offer creation");
		navigateToAction(new CreateOfferViewModel(navigateToAction));
	}
}
